﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context.Configurations {
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode> {

        public void Configure(EntityTypeBuilder<PromoCode> builder) {

            // У промокода записан Id сотрудника, который его породил
            builder.HasOne(x => x.PartnerManager)
                 .WithMany(x => x.PromoCodes)
                 .HasForeignKey(x => x.PartnerManagerId)
                 .IsRequired();

            // У промокода фиксируется 1 клиент, которому он может быть назначен
            builder.HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(p => p.CustomerId)
                .IsRequired(false);

            // У промокода фиксируется 1 тип предпочтения, к которому он относится
            builder.HasOne(p => p.Preference)
                .WithMany(pf => pf.PromoCodes)
                .HasForeignKey(p => p.PreferenceId)
                .IsRequired();

            builder.Property(p => p.Code).HasMaxLength(100);
            builder.Property(p => p.ServiceInfo).HasMaxLength(200);
        }
    }
}
