﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context.Configurations {
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee> {

        public void Configure(EntityTypeBuilder<Employee> builder) {

            // У каждого сотрудника гарантированно проставлена одна роль
            builder.HasOne(e => e.Role)
                .WithMany(r => r.Employees)
                .HasForeignKey(e => e.RoleId)
                .IsRequired();

            builder.Property(e => e.FirstName).HasMaxLength(30);
            builder.Property(e => e.LastName).HasMaxLength(30);
            builder.Property(e => e.Email).HasMaxLength(50);
        }
    }
}
