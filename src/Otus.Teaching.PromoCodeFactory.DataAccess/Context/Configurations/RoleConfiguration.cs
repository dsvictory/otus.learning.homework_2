﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context.Configurations {
    public class RoleConfiguration : IEntityTypeConfiguration<Role> {

        public void Configure(EntityTypeBuilder<Role> builder) {
            builder.Property(r => r.Name).HasMaxLength(30);
            builder.Property(r => r.Description).HasMaxLength(200);
        }
    }
}
