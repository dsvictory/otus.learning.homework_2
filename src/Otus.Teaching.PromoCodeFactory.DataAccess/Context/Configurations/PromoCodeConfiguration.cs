﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context.Configurations {
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer> {

        public void Configure(EntityTypeBuilder<Customer> builder) {

            // Удаляем промокоды при удалении клиента
            builder.HasMany(x => x.PromoCodes)
                .WithOne(x => x.Customer)
                .HasForeignKey(x => x.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(p => p.FirstName).HasMaxLength(30);
            builder.Property(p => p.LastName).HasMaxLength(30);
            builder.Property(p => p.Email).HasMaxLength(50);
        }
    }
}
