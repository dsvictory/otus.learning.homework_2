﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context.Configurations {
    public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference> {

        public void Configure(EntityTypeBuilder<CustomerPreference> builder) {

            // Ключ
            builder.HasKey(x => new { x.CustomerId, x.PreferenceId });

            // Связка с клиентом
            builder.HasOne(cp => cp.Customer)
                 .WithMany(c => c.CustomerPreferences)
                 .HasForeignKey(cp => cp.CustomerId)
                 .IsRequired();

            // Связка с предпочтением
            builder.HasOne(cp => cp.Preference)
                 .WithMany(p => p.CustomerPreferences)
                 .HasForeignKey(cp => cp.PreferenceId)
                 .IsRequired();
        }
    }
}

