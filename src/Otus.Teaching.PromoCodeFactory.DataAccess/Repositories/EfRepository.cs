﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories {
    public class EfRepository<T> : IRepository<T> where T : BaseEntity {

        private readonly ApplicationDbContext _dataContext;

        public EfRepository(ApplicationDbContext dbContext) {
            _dataContext = dbContext;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(Func<T, bool> filter = null) {
            if (filter != null) {
               return await Task.FromResult(_dataContext.Set<T>().Where(filter));
            }
            return await Task.FromResult(_dataContext.Set<T>().AsEnumerable());
        }

        public virtual async Task<T> GetByIdAsync(Guid id) {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task AddAsync(T entity) {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public virtual async Task UpdateAsync(T entity) {
            _dataContext.Attach(entity);
            _dataContext.Entry(entity).State = EntityState.Modified;
            await _dataContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(T entity) {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public virtual async Task RemoveAsync(T entity) {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
    }
}