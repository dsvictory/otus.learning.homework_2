﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context {
    public static class ApplicationDbContextInitializer {

        public static void Initialize(ApplicationDbContext context) {
            //context.Database.EnsureDeleted();
            //context.Database.EnsureCreated();

            context.Roles.AddRange(FakeDataFactory.Roles);
            context.Preferences.AddRange(FakeDataFactory.Preferences);
            context.Employees.AddRange(FakeDataFactory.Employees);
            context.Customers.AddRange(FakeDataFactory.Customers);
            context.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);

            context.SaveChanges();
        }
    }
}
