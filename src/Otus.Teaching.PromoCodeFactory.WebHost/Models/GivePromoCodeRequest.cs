﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string ExpirationDate { get; set; }

        public Guid PartnerManagerId { get; set; }

        public Guid PreferenceId { get; set; }  
    }
}