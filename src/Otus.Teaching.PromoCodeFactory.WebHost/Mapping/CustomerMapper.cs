﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping {
    public static class CustomerMapper {

        public static Customer MapFromModel(CustomerCreateOrEditRequest request,
            IEnumerable<Preference> preferences, Customer customer = null) 
        {
            if (customer == null) {
                customer = new Customer();
            }

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;

            customer.CustomerPreferences?.Clear();
            if (preferences != null && preferences.Any()) {    
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference() {
                    Customer = customer,
                    Preference = x
                }).ToList();
            }

            return customer;
        }

    }
}
