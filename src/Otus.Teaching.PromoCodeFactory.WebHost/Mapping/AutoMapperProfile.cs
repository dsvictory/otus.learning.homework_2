﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping {
    public class AutoMapperProfile : Profile {

        public AutoMapperProfile() {

            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Preference, PreferenceResponse>();
            CreateMap<Customer, CustomerResponse>()
                .ForMember(x => x.Preferences, opt => opt.MapFrom(x => x.CustomerPreferences.Select(x => x.Preference)));

            CreateMap<Preference, PreferenceResponse>();

            CreateMap<PromoCode, PromoCodeShortResponse>()
                .ForMember(x => x.BeginDate, opt => opt.MapFrom(x => x.BeginDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(x => x.EndDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.PartnerName, opt => opt.MapFrom(x => x.PartnerManager.FullName));

            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(x => x.BeginDate, opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(x => DateTime.Parse(x.ExpirationDate)));

        }
    }
}

