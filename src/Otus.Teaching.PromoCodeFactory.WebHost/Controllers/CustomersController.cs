﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomersController(
            IRepository<Customer> CustomerRepository,
            IRepository<Preference> preferenceRepository,
            IMapper mapper) {
            _customerRepository = CustomerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение краткой информации по всем клиентам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync() {
            var customers = await _customerRepository.GetAllAsync();
            return _mapper.Map<List<CustomerShortResponse>>(customers);
        }

        /// <summary>
        /// Получение полной информации по конкретному клиенту
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id) {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            return _mapper.Map<CustomerResponse>(customer);
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <param name="request">Параметры клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CustomerCreateOrEditRequest request) {
            try {
                if (!ModelState.IsValid) {
                    return BadRequest(ModelState);
                }

                IEnumerable<Preference> preferences = null;
                if (request.PreferenceIds != null && request.PreferenceIds.Any()) {
                    preferences = await _preferenceRepository.GetAllAsync(x => request.PreferenceIds.Contains(x.Id));
                }

                var newCustomer = CustomerMapper.MapFromModel(request, preferences);

                await _customerRepository.AddAsync(newCustomer);

                return Ok();
            }
            catch (Exception ex) {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Редактирование существующего в системе клиента
        /// </summary>
        /// <param name="request">Параметры клиента</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditCustomersAsync(CustomerCreateOrEditRequest request) {
            try {
                if (!ModelState.IsValid) {
                    return BadRequest(ModelState);
                }

                var existingCustomer = await _customerRepository.GetByIdAsync(request.Id);

                IEnumerable<Preference> preferences = null;
                if (request.PreferenceIds != null && request.PreferenceIds.Any()) {
                    preferences = await _preferenceRepository.GetAllAsync(x => request.PreferenceIds.Contains(x.Id));
                }

                var customerForUpdating = CustomerMapper.MapFromModel(request, preferences, existingCustomer);

                await _customerRepository.UpdateAsync(customerForUpdating);

                return Ok();
            }
            catch (Exception ex) {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id) {
            try {
                var customer = await _customerRepository.GetByIdAsync(id);
                if (customer == null) {
                    return NotFound();
                }

                await _customerRepository.RemoveAsync(customer);

                return Ok();
            }
            catch (Exception ex) {
                return StatusCode(500, ex.Message);
            }
        }
    }
}