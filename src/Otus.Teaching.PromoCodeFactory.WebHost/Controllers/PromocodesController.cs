﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase {

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IMapper _mapper;


        public PromocodesController(IRepository<PromoCode> promocodeRepository, 
            IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository,
            IRepository<Employee> employeeRepository, IMapper mapper) 
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            return _mapper.Map<List<PromoCodeShortResponse>>(promocodes);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиенту с указанным в промокоде предпочтением
        /// </summary>
        /// <returns>Параметры промокода</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            try {
                var promocode = _mapper.Map<PromoCode>(request);

                var partner = _employeeRepository.GetByIdAsync(request.PartnerManagerId);

                var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);

                var customers = await _customerRepository.GetAllAsync();

                // Прокод назначаем рандомному одному клиенту с предпочтением как у промокода
                var customer = customers.Where(
                    (x, i) =>
                        i == new Random().Next(0, customers.Count())
                        && x.CustomerPreferences.Select(x => x.PreferenceId).Contains(preference.Id)
                )?.First();

                if (customer != null) {
                    customer.PromoCodes.Add(promocode);
                    await _customerRepository.UpdateAsync(customer);
                    return Ok();
                }
                else {
                    return NotFound("There is no client with such preference!");
                }
            }
            catch (Exception ex) {
                return StatusCode(500, ex.Message);
            }      

        }
    }
}